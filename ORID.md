## **O**：

-  First, there was a code review session every morning. We reviewed the code related to JPA that I wrote yesterday. In fact, our team has also privately discussed the advantages and disadvantages of Mybatis and JPA, and learned the similarities and differences between the two.
-  Then in class, I learned about Flyway. It is a new tool that I had never learned before. It is a tool for managing and controlling the versions of SQL statements for databases. It is mainly for the traceability of statements such as DDL and DML.
-  Today, we also had a special retro. We reviewed some well and notwell behaviors of our team members during this period. My own problem is that I did not think enough in the process of completing the task.
-  After that, we also had a group session, where we learned about microservices frameworks, container technology, and cloud native. Although many of the concepts are still unfamiliar, such as Laas, Paas, and Kubernetes, we still gained a lot.



## **R：**

- nice weekend


## **I：**

- We have learned many new concepts in the classroom, and although some have been around for many years, we still have little understanding of them.


## **D：**

- Learn to search for information and solve problems when a problem gets stuck
