package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final EmployeeJPARepository employeeJPARepository;
    private final CompanyJPARepository companyJPARepository;

    public CompanyService(EmployeeJPARepository employeeJPARepository, CompanyJPARepository companyJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
        this.companyJPARepository = companyJPARepository;
    }

    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return companyJPARepository.findAll(pageRequest).getContent();
    }

    public Company findById(Long id) {
        return companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public Company update(Long id, Company company) {
        Company optionalCompany = findById(id);
        if (company.getName() != null) {
            optionalCompany.setName(company.getName());
        }
        return companyJPARepository.save(optionalCompany);
    }

    public Company create(Company company) {
        return companyJPARepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}

