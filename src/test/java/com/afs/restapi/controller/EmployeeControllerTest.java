package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_employee_age_and_salary() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        Employee employeeUpdateRequest = new Employee(null, "zhangsan", 24, "Male", 10001);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);
        mockMvc.perform(put("/employees/{id}", savedEmployee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeUpdateRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeUpdateRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeUpdateRequest.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());

    }

    @Test
    void should_create_employee() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(request.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(request.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }

    @Test
    void should_find_employees() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeRequest.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employee_by_id() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeRequest.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }

    @Test
    void should_delete_employee_by_id() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(delete("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(savedEmployee.getId()).isEmpty());
    }

    @Test
    void should_find_employee_by_gender() throws Exception {
        EmployeeRequest employeeRequest = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeRequest.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employees_by_page() throws Exception {
        EmployeeRequest employeeZhangsan = getEmployeeRequestZhangsan();
        EmployeeRequest employeeSusan = getEmployeeRequestSusan();
        EmployeeRequest employeeLisi = getEmployeeRequestLisi();
        Employee savedEmployeeZhangsan = employeeJPARepository.save(EmployeeMapper.toEntity(employeeZhangsan));
        Employee savedEmployeeSusan = employeeJPARepository.save(EmployeeMapper.toEntity(employeeSusan));
        employeeJPARepository.save(EmployeeMapper.toEntity(employeeLisi));

        mockMvc.perform(get("/employees")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployeeZhangsan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeZhangsan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeZhangsan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeZhangsan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedEmployeeSusan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeSusan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeSusan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeSusan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").doesNotExist());
    }

    private static EmployeeRequest getEmployeeRequestZhangsan() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }


    private static EmployeeRequest getEmployeeRequestSusan() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestLisi() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }
}